package counter;

public class Counter {

	protected int count;
	
	public Counter() {
		count = 0;
	}
	
	public int read() {
		return count;
	}
	
	public void increment() {
		count++;
	}
}
