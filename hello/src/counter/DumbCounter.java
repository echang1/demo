package counter;

public class DumbCounter extends Counter {
	
	protected boolean hasBeenReset;
	
	public DumbCounter() {
		super();
		hasBeenReset = false;
	}
	
	public void reset() {
		hasBeenReset = true;
		count = 0;
	}
	@Override
	public int read() {
		if (hasBeenReset) {
			return count;
		} else {
			throw new UnresetCounterException();
		}
			

	}
	
}
